export default defineNuxtConfig({
  modules: [
    '@nuxtjs/tailwindcss',
  ],
  runtimeConfig: {
    public: {
      apiBase: 'https://rdtest.ml'
    }
  },
  build: {
    ...(process.env.NODE_ENV !== 'production'
        ? {}
        : {
          postcss: {
            tailwindcss: {},
            plugins: {
              '@fullhuman/postcss-purgecss': {
                content: [
                  'components/**/*.vue',
                  'layouts/**/*.vue',
                  'pages/**/*.vue',
                  'plugins/**/*.js',
                ],
                styleExtensions: ['.css'],
                keyframes: true,
                safelist: {
                  standard: [
                    'body',
                    'html',
                    'nuxt-progress',
                    /col-/,
                    /-(leave|enter|appear)(|-(to|from|active))$/,
                    /^(?!(|.*?:)cursor-move).+-move$/,
                    /^router-link(|-exact)-active$/,
                    /data-v-.*/,
                    /v-data-table.*/,
                    /v-data-table--mobile/,
                    /lazyload/
                  ],
                  deep: [
                    /page-enter/,
                    /page-leave/,
                    /dialog-transition/,
                    /tab-transition/,
                    /tab-reversetransition/,
                    /v-progress-circular/,
                    /-(indeterminate|loading)$/,
                    /--mobile$/,
                    /v-input__prepend-inner/
                  ]
                }
              },
              'css-byebye': {
                rulesToRemove: [
                  /.*\.v-application--is-rtl.*/
                ]
              }
            }
          }
        }
    ),
  },
  css: [
    '@/assets/css/main.css',
  ],
})
